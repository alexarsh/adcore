module.exports = {
  files: {
    javascripts: {
      joinTo: {
        'vendor.js': /^node_modules/,
        'app.js': /^app/
      }
    },
    stylesheets: {
        joinTo: 'app.css'
    }
  },
  plugins: {
    babel: {
      presets: ['es2015']
    }
  },
  watcher: {
    usePolling: true
  }
}