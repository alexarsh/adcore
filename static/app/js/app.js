//Import libraries
import Vue from 'vue/dist/vue.js'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

//Import components
// import Login from './components/login'
import Root from './components/root'

//Using libraries
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueResource);

//Handle csrf and auth
Vue.http.interceptors.push(function(request) {
  request.headers.set('X-CSRFToken', document.body.querySelector('input[name="csrfmiddlewaretoken"]').value);
  // if (localStorage.getItem("token")) {
  //     request.headers.set('Authorization', 'JWT ' + localStorage.getItem("token"));
  // }
});

//Defining router
var router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: Root, name: 'root' },
        // { path: '/login', component: Login, name: 'login' },
    ]
});

//Handling login redirections
// router.beforeEach((to, from, next) => {
//     if (to.path != "/login" && !localStorage.getItem("token")) {
//         next("/login");
//     }
//     else {
//         next();
//     }
// });

//Defining vuex store
var store = new Vuex.Store({
    state: {
        data: {}
    },
    mutations: {
        set_initial_data(state, data) {
            state.data = data;
        }
    },
    actions: {
        set_initial_data({commit}, data) {
            commit("set_initial_data", data);
        }
    },
    getters: {
        data: state => {
            return state.data
        }
    }
});

//Starting the app
var app = new Vue({
    router,
    store,
    mounted: function() {
        var that = this;
        this.$http.get("/initial_data/").then(function (data) {
            console.log(data.data);
            that.$store.dispatch('set_initial_data', data.data);
        });
    }
}).$mount('#app');
