import Vue from 'vue/dist/vue.js'

var Header = Vue.component('my-header', {
    template: `<nav>
            <div class="nav-wrapper">
          <a href="#" class="brand-logo"><router-link to="/">Courses</router-link></a>
        </div>
    </nav>`,
    methods: {
        // logout: function () {
        //     var that = this;
        //     this.$http.get("/api-auth/logout/", {}).then(function (data) {
        //         that.$store.dispatch('logout').then(() => {
        //             that.$router.push("/login");
        //         });
        //     });
        // }
    },
    computed: {
        // isLoggedIn() {
        //     return this.$store.getters.isLoggedIn;
        // },
        onLandingPage() {
            return this.$router.currentRoute.path == "/";
        },
        // showLogin() {
        //     return this.$router.currentRoute.path != "/login" && !this.$store.getters.isLoggedIn;
        // }
    }
});

export default Header;
