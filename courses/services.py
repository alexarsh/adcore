def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'can_manage_users': user.role == "Admin" or user.role == "User manager",
        'can_manage_timezones': user.role == "Admin" or user.role == "Timezones manager"
    }