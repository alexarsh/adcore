from rest_framework import permissions


class UserManagingPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.role == "Admin" or request.user.role == "User manager"
        

class TimezonesManagingPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_anonymous:
            return False
        return request.user.role == "Admin" or request.user.role == "Timezones manager"