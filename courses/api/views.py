from rest_framework import viewsets, permissions
from courses.models import Course

from django.db.models import Q

from .serializers import CourseSerializer
from .permissions import *
    
class CourseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    # permission_classes = [TimezonesManagingPermission]
    
    def get_queryset(self):
        queryset = Course.objects.all()
        filter_by_name = self.request.query_params.get('query', None)
        if filter_by_name is not None:
            queryset = queryset.filter(Q(name__contains=filter_by_name) | 
                Q(country__contains=filter_by_name) |
                Q(city__contains=filter_by_name) |
                Q(university__contains=filter_by_name) |
                Q(start__contains=filter_by_name))
        return queryset
        