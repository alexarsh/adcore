from datetime import datetime
from geopy.geocoders import Nominatim
import pytz
from tzwhere import tzwhere

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from .utils import *
        
class Course(models.Model):
    name = models.CharField(max_length = 1024)
    description = models.CharField(max_length = 1024, blank=True, null=True)
    university = models.CharField(max_length = 1024)
    city = models.CharField(max_length = 128)
    country = models.CharField(max_length = 128)
    start = models.DateField()
    end = models.DateField()
    price = models.PositiveIntegerField(null=True, blank=True)
    currency = models.CharField(max_length = 128, null=True, blank=True)    
    
    # def clean(self):
    #     location = Nominatim(timeout=20).geocode(self.city_name)
    #     if not location:
    #         raise ValidationError(_('Your city name is wrong'))
    #     if self.name != tzwhere.tzwhere().tzNameAt(location.latitude, location.longitude):
    #         raise ValidationError(_('%s is not in %s timezone') % (self.city_name, self.name))
    #     super().clean()
    # 
    # def save(self, *args, **kwargs):
    #     self.clean()
    #     self.gmt_difference = datetime.now(pytz.timezone(self.name)).strftime('%z')
    #     super().save(*args, **kwargs)  # Call the "real" save() method.
    
    def __str__(self):
        return u'%s' % self.name
        