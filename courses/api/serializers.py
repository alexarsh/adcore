from datetime import datetime
import pytz
from forex_python.converter import CurrencyCodes

from rest_framework import serializers

from courses.models import Course
        

class CourseSerializer(serializers.HyperlinkedModelSerializer):
    location = serializers.SerializerMethodField()
    length = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = Course
        fields = ('pk', 'url', 'name', 'location', 'description', 'university', 'city',
            'country', 'start', 'end', 'length', 'price', 'currency')
    
    def get_location(self, obj):
        return '%s, %s, %s' % (obj.country, obj.city, obj.university)
        
    def get_length(self, obj):
        return (obj.end - obj.start).days
        
    def get_price(self, obj):
        sign = CurrencyCodes().get_symbol(obj.currency) or ''
        return "%s %s" % (sign, obj.price or '')
        
    