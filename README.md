# Courses app

Webapp for managing different cities in different timezones

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
In order to install the project, you will need the latest python3 with pip and yarn to be installed globally.
(Maybe you will also need python2 executable in order to compile node-sass on specific OSes).
```

### Installing

Here are step by step install instructions:

```
1. Clone the Project
2. From project dir run: 'pip install -r requirements.txt'
3. From the static dir run: 'yarn install'
4. From the static dir run: 'brunch build'
5. From project dir run: 'python manage.py migrate'
6. From project dir run: 'python manage.py runserver'
```

## Running the tests

In order to run the unit tests, go to the project dir and run:
```
python manage.py test
```

## Deployment

If you would like to deploy the project you can use, for example, Nginx+Gunicron+WSGI.
There is a wsgi config file in adcore directory.

## Built With

* [Django](https://www.djangoproject.com/) - The server web framework used
* [Vuejs](https://vuejs.org/) - The client web framework used
* [Yarn](https://yarnpkg.com/en/) - JS/CSS Dependency Management
* [Brunch](http://brunch.io/) - JS/CSS build tool

## Authors

* **Arshavski Alexander** - [Sourcerer](https://sourcerer.io/alexarsh)
