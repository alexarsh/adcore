import Vue from 'vue/dist/vue.js'

import Header from './header'

var Login = Vue.component('root', {
    template: `<div>
        <my-header></my-header>
        <div id="login-form">
            <form class="login-form">
                <div class="form-group">
                    <input type="text" v-bind:class="valid_credentials" name="username" value="" placeholder="Enter your username" id="login-name" ref="username"/>
                    <label class="login-field-icon fui-user" for="login-name"></label>
                </div>

                <div class="form-group">
                    <input type="password" v-bind:class="valid_credentials" name="password" value="" placeholder="Password" id="login-pass" ref="password"/>
                    <label class="login-field-icon fui-lock" for="login-pass"></label>
                </div>

                <a class="btn btn-primary btn-lg btn-block" href="#" v-on:click="login">Log in</a>
                <a class="btn btn-primary btn-lg btn-block signup" href="#" v-on:click="signup">Sign up</a>
            </form>
        </div>
    </div>`,
    components: {'my-header': Header},
    data() {
        return {
            valid: true
        }
    },
    methods: {
        login: function () {
            var that = this;
            this.$http.post("/api-token-auth/", {"username": this.$refs.username.value, "password": this.$refs.password.value}).then(function (data) {
                var token = data.body['token'];
                if (token) {
                    document.cookie = document.cookie + "; sessionid= " + token;
                    that.valid = true;
                    that.$store.dispatch("login", {"token":token, 
                                                   "can_manage_users": data.body['can_manage_users'], 
                                                   "can_manage_timezones": data.body['can_manage_timezones']}).then(() => {
                        that.$router.push("/");
                    });
                }
                else {
                    that.valid = false;
                }
            }, function (error) {
                that.valid = false;
            });
        },
        signup: function () {
            if (this.$refs.username.value == "" || this.$refs.password.value == "") {
                this.valid = false;
            }
            else {
                var that = this;
                var payload = {"username": this.$refs.username.value, 
                                "password": this.$refs.password.value, 
                                "password_confirm": this.$refs.password.value, 
                                "role": "Timezones manager"};
                this.$http.post("/accounts/register/", payload).then(function (data) {
                    that.login();
                }, function (error) {
                    that.valid = false;
                });
            }
        }
    },
    computed: {
        valid_credentials: function () {
            return {
                'has-error': !this.valid
            }
        }
    }
});

export default Login;
