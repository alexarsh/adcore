import Vue from 'vue/dist/vue.js'

import Header from './header'
// import UsersTable from './users_table'
import CoursesTable from './courses_table'

var Root = Vue.component('root', {
    template: `<div> 
            <my-header></my-header> 
            <courses-table></courses-table> 
        </div>`
    ,
    components: {
        'my-header': Header,
        'courses-table': CoursesTable
    },
    data() {
        return {
            name: "",
            email: "",
            message: "",
            valid: true
        }
    },
    methods: {
        send_email: function () {
            var that=this;
            $.post("/send_email/", $('#contact-us').serialize(), function(data) {
                that.name = "";
                that.email = "";
                that.message = "";
            })
        }
    },
    computed: {
        check_email: function () {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            this.valid = !this.email || pattern.test(this.email);
            return {
                'has-error': !this.valid
            }
        },
        can_send: function () {
            return {
                'disabled': !(this.valid && this.name)
            }
        },
        can_manage_users: function () {
            return localStorage.getItem("can_manage_users") == "true" ? true : false;
        },
        can_manage_timezones: function () {
            return localStorage.getItem("can_manage_timezones") == "true" ? true : false;
        }
    }
})

export default Root;
