from django.contrib import admin
# from rest_framework_jwt.views import obtain_jwt_token
# from django.contrib.auth import views as login_views
from django.urls import include, path, re_path

from courses import views

urlpatterns = [
    path('api/', include('courses.urls')),
    # path('api-auth/', include('rest_framework.urls')),
    # path('api-token-auth/', obtain_jwt_token),
    # path('accounts/', include('rest_registration.api.urls')),
    path('admin/', admin.site.urls),
    path('initial_data/', views.initial_data),
    # path('logout/', login_views.LogoutView.as_view()),
    re_path(r'', views.main),
]
