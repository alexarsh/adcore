from django.apps import AppConfig


class TimezonesConfig(AppConfig):
    name = 'timezones'
