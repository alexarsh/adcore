import Vue from 'vue/dist/vue.js'
import {ServerTable, Event} from 'vue-tables-2'
import VModal from 'vue-js-modal'
import vSelect from 'vue-select'
import VueMoment from 'vue-moment'
import moment from 'moment'
import Datepicker from 'vuejs-datepicker';

Vue.use(VueMoment, { moment, })
Vue.use(VModal);
Vue.use(ServerTable);
Vue.component('v-select', vSelect);

var CoursesTable = Vue.component('courses-table', {
    components: {
        Datepicker
    },
    template: `<div>
                    <h2>Courses list</h2>
                    <v-server-table url="/api/courses/" :columns="columns" :options="options" ref="mycoursestable">
                        <div slot="actions" slot-scope="props">
                            <i class="fa fa-pencil-square-o" v-on:click="edit"></i>
                            <i class="fa fa-times" v-on:click="remove"></i>
                        </div>
                    </v-server-table>
                    <a class="btn btn-floating btn-large waves-effect waves-light add" v-on:click="add"><i class="material-icons">+</i></a>
                    <modal name="courses_modal" :height="600" ref="modal">
                        <div class="row">
                            <form class="col s12">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input v-model="name" id="name" placeholder="Course name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input v-model="university" id="university" placeholder="University">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input v-model="country" id="country" placeholder="Country">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input v-model="city" id="city" placeholder="City">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <v-select max-height="200px" v-model="currency" id="currency" :options="currencies"></v-select>
                                    </div>
                                    <div class="input-field col s6">
                                        <input v-model="price" id="price" placeholder="Price">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <datepicker v-model="start" id="start" placeholder="Start Date"></datepicker>
                                    </div>
                                    <div class="input-field col s1">
                                        <i class="fa fa-2x fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s11">
                                        <datepicker v-model="end" id="end" placeholder="End Date"></datepicker>
                                    </div>
                                    <div class="input-field col s1">
                                        <i class="fa fa-2x fa-calendar"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea v-model="description" id="description" class="materialize-textarea" placeholder="Course description"></textarea>
                                    </div>
                                </div>
                                <a class="waves-effect waves-light btn" v-on:click="save">Save</a>
                                <a class="waves-effect waves-light btn" v-on:click="close">Close</a>
                                <div class="row errors" ref="errors"></div>
                                <div class="progress" ref="progress">
                                    <div class="indeterminate"></div>
                                </div>
                            </form>
                        </div>
                    </modal>
                </div>`,
    data() {
        return {
            columns: ['name', 'location', 'start', 'length', 'price', 'actions'],
            options: {
                filterable: true,
                responseAdapter: function(resp) { 
                    var data = this.getResponseData(resp);
                    return { data: data.results, count: data.count } 
                }
            },
            name: "",
            country: "",
            university: "",
            city: "",
            price: "",
            currency: "",
            start: "",
            end: "",
            description: "",
            pk: null,
            edit_obj: true,
            valid: true
        }
    },
    methods: {
        edit: function(event) {
            var row_index = event.target.parentElement.parentElement.parentElement.rowIndex - 1;
            this.pk = this.$refs.mycoursestable.data[row_index].pk;
            this.$http.get("/api/courses/" + this.pk + "/", {}).then(function (data) {
                this.name = data.data["name"];
                this.country = data.data["country"];
                this.university = data.data["university"];
                this.city = data.data["city"];
                this.price = data.data["price"].split(" ")[1];
                this.currency = data.data["price"].split(" ")[0];
                this.start = data.data["start"];
                this.end = data.data["end"];
                this.description = data.data["description"];
                this.edit_obj = true;
                this.$modal.show('courses_modal');
            }, function (error) {});
        },
        add: function(event) {
            this.edit_obj = false;
            this.name = "";
            this.country = "";
            this.university = "";
            this.city = "";
            this.price = "";
            this.currency = "";
            this.start = "";
            this.end = "";
            this.description = "";
            this.$modal.show('courses_modal');
        },
        close: function() {
            this.$modal.hide('courses_modal');
        },
        save: function() {
            this.$refs.progress.style.display = "block";
            var payload = {name: this.name, country: this.country,
                university: this.university, city: this.city,
                price: this.price, currency: this.currency.value,
                start: this.format_date(this.start), end: this.format_date(this.end), 
                description: this.description};
            if (this.edit_obj) {
                this.$http.put("/api/courses/" + this.pk + "/", payload).then(function (data) {
                    this.$refs.errors.innerHTML = "";
                    this.$refs.mycoursestable.refresh();
                    this.valid = true;
                    this.$refs.progress.style.display = "none";
                    this.$modal.hide('courses_modal');
                }, function (error) {
                    this.valid = false;
                    this.$refs.errors.innerHTML = "";
                    for (var i in error.data) {
                        this.$refs.errors.innerHTML += "" + i + ": " + error.data[i] + "<br/>";
                    }
                    this.$refs.progress.style.display = "none";
                });
            }
            else {
                this.$http.post("/api/courses/", payload).then(function (data) {
                    this.$refs.errors.innerHTML = "";
                    this.$refs.mycoursestable.refresh();
                    this.valid = true;
                    this.$refs.progress.style.display = "none";
                    this.$modal.hide('courses_modal');
                }, function (error) {
                    this.valid = false;
                    this.$refs.errors.innerHTML = "";
                    for (var i in error.data) {
                        this.$refs.errors.innerHTML += "" + i + ": " + error.data[i] + "<br/>";
                    }
                    this.$refs.progress.style.display = "none";
                });
            }
        },
        remove: function() {
            var row_index = event.target.parentElement.parentElement.parentElement.rowIndex - 1;
            this.pk = this.$refs.mycoursestable.data[row_index].pk;
            this.$http.delete("/api/courses/" + this.pk + "/", {}).then(function (data) {
                this.$refs.mycoursestable.refresh();
            }, function (error) {});
        },
        format_date(my_date) {
            if (typeof my_date == "string") {
                return my_date;
            }
            return my_date.getFullYear() + '-' + my_date.getMonth() + '-' + my_date.getDate();
        }
    },
    computed: {
        currencies () {
            return this.$store.getters.data["currencies"] || [];
        }
    }  
});

export default CoursesTable;
