import json

from django.contrib.auth.hashers import make_password
from django.test import TestCase

from .models import *


class TimezonesTestCase(TestCase):
    
    def setUp(self):    
        User.objects.create(username="t_manager", password=make_password("123"), role="Timezones manager")

    def get_token(self):
        response = self.client.post('/api-token-auth/', {'username': "t_manager", 'password': "123"})
        return json.loads(response.content)["token"]
        
    def test_login(self):
        """Test Authentication"""
        response = self.client.post('/api/timezones/', {'name': "Asia/Jerusalem", 'city_name': "Jerusalem"})
        self.assertEqual(response.status_code, 401) # Not logged in
        token = self.get_token()
        response = self.client.post('/api/timezones/', {'name': "Asia/Jerusalem", 'city_name': "Jerusalem"})
        self.assertEqual(response.status_code, 401) # No token provided
        
    def test_permissions(self):
        """Test Permissions"""
        token = self.get_token()
        response = self.client.post('/api/users/', {'username': "test", 'password': "test", "role": "Timezones manager"}, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(json.loads(response.content)["detail"], "You do not have permission to perform this action.") # Don't have permissions
        
    def test_timezones(self):
        """Test Timezones"""
        token = self.get_token()
        response = self.client.post('/api/timezones/', {'name': "Asia/Jerusalem", 'city_name': "Jerusalem"}, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, 201) # Timezone created
        response = self.client.post('/api/timezones/', {'name': "Asia/Jerusalem", 'city_name': "wrong_city_name"}, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["non_field_errors"][0], "Your city name is wrong") # Wrong city name
        response = self.client.post('/api/timezones/', {'name': "Asia/Jerusalem", 'city_name': "New York"}, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["non_field_errors"][0], 'New York is not in Asia/Jerusalem timezone') # City not in timezone